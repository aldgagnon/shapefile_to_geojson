## shapefile to geojson converter

Converts shapefiles into RFC 7946 compliant GeoJSON.

## Usage

1. Place all shapefiles into the input directory
1. Run the container: `docker-compose up --build shapefile_to_geojson`
1. Navigate to the output directory for the GeoJSON output