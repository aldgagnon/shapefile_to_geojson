#!/bin/sh

# title: Shapefile to GeoJSON converter
# description: Convert shapefiles to GeoJSON (RFC 7946 standard)
# author: Andrew Gagnon
# version: 0.2

for f in /data/input/*.shp; do
	FILENAME=$(basename -- "$f" .shp)
	ogr2ogr -f GeoJSON /data/output/$FILENAME.geojson $f -lco RFC7946=YES
done